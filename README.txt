
General Information
-------------------
Anthingslider

Anything slider lets you create the slideshow gallery to showcase featured content.

Anything slider gives administrators three important tools:
1) A simple method of adding slides to the slideshow.
2) An administration interface to configure slideshow settings.
3) Simple slider positioning using the Drupal block system.


